#
#
#
PACKAGE		=  shift_guide
VERSION		:= $(shell \
		     grep "<span.*>Version [0-9].[0-9]*</span>" index.html | \
		     sed 's/.*Version \([^<]*\)<.*/\1/')
dist:	shift_guide.pdf 
	mkdir -p $(PACKAGE)
	cp fmd_all.png index.html $(PACKAGE)/
	cp -a images panels screens $(PACKAGE)/
	cp $(PACKAGE).pdf $(PACKAGE)/
	tar --exclude=CVS --exclude=*~ \
		-cvzf $(PACKAGE)-$(VERSION).tar.gz $(PACKAGE)
	rm -rf $(PACKAGE)

shift_guide.pdf:	$(HOME)/PDF/ALICE_FMD_Shift_Guide.pdf
	@if test $< -nt index.html ; then \
		echo "Getting newer PDF" ; \
		cp $< $@ ; fi 

index.pdf:index.html 
	unoconv $< 

show:
	@echo "$(PACKAGE)-$(VERSION)"

toc.html:	index.html
	grep -e "<[hH][0-5]><a name=" index.html | 		\
	    sed -e 's/^[[:space:]]*<[hH]2>/      <li>/' 	\
	        -e 's/^[[:space:]]*<[hH]3>/        <li>/' 	\
		-e 's/^[[:space:]]*<[hH]4>/          <li>/' 	\
	        -e 's/name="/href="#/'				\
	        -e 's/<\/a>//'					\
	        -e 's/<\/[hH][2-4]>/<\/a><\/li>/'		\
		> $@



alt_shift_guide.pdf:	index.html
	htmldoc -f $@				\
		--book				\
		--bottom 1.5cm			\
		--color	 			\
		--duplex			\
		--embedfonts			\
		--footer " / "			\
		--header "c h"			\
		--left 2cm			\
		--links				\
		--no-toc			\
		--numbered			\
		--right 2cm			\
		--top	2cm			\
		--verbose			\
		$<

