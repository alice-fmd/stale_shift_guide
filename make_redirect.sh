out=redirect

make_one()
{
    anchor=$1
    file=$2
    cat <<EOF >> ${file}.html
<!-- $file generated -->
<html>
  <head>
    <title>FMD $node panel</title>
    <meta http-equiv="refresh" content="1;url=http://alifmdwn002/fmd/shift_guide/#panels_FMD_${anchor}">
  </head>
  <body>
    <p>You will be redirected to the FMD <a href="http://alifmdwn002/fmd/shift_guide/#{anchor}">shift guide</a> in a second ...</p>
  </body>
</html>
EOF
}

dir=$1 
if test "x$dir" = "x" ; then dir=panels ; fi 
rm -rf $out 
mkdir -p $out

echo "Input directory is '$dir'"
for i in $dir/*.png ; do 
    n=`basename $i .png | sed 's/FMD_//'` 
    node=FMD_${n}
    case $n in 
	subdetector) file="FMD_Detector" ;; 
	infrastructure) file="FMD_INFRASTRUCTURE"  ;; 
	48V_PS) node="FMD_EasyPS" ;; 
	cooling) node="FMDRingCooling" ;; 
	RCU_state) node="DIMRCU";;
	miniconf) node="FMD_Miniconf" ;; 
	pedconf) node="FMD_Pedconf" ;;
	dig_fec) node="DIMFEC" ;;
	half_ring) node="FMD_Sector" ;; 
	*) ;;
    esac
    printf "Processing %30s -> %s\n" $n $node

    make_one "$n" "$out/$node"
done


	    
	    
